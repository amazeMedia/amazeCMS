require('dotenv').config();

/* use without a webserver */
var mongoose = require('mongoose'),
    keystone = require('keystone');

// keystone.connect(mongoose,app);

keystone.import('models');

mongoose.connect('localhost', 'amaze-cms');

mongoose.connection.on('open', function() {
    console.log('opened');
    // ready to do things, e.g.
    var Page = keystone.list('Page');

    // new User.model().save();
    keystone.createItems({

      Page: [{
        title: 'Test 1',
        'content.brief': 'This is an example draft post.'
      }, {
        title: 'Test 2',
        state: 'published',
        'content.brief': 'This post has been published!',
      }]

    }, function(err, stats) {
      stats && console.log(stats.message);
      done(err);
    });
});
