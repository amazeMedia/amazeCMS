var keystone = require('keystone');

exports.create = {

	Page: [{
		title: 'Test 1',
		'content.brief': 'This is an example draft post.'
	}, {
		title: 'Test 2',
		state: 'published',
		'content.brief': 'This post has been published!',
	}]

};
