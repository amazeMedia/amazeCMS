var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Gallery Model
 * =============
 */

var Gallery = new keystone.List('Gallery', {
	autokey: { from: 'naam', path: 'key', unique: true },
	label: 'Afbeeldingen',
	singular: 'Afbeelding',
	plural: 'Afbeeldingen',
});

Gallery.add({
	naam: { type: String, required: true, initial: false },
	publicatieDatum: { type: Date, default: Date.now },
	enkeleAfbeelding: { type: Types.CloudinaryImage },
	afbeeldingen: { type: Types.CloudinaryImages },
});

Gallery.defaultColumns = 'naam, publicatieDatum';

Gallery.register();
