var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * PostCategory Model
 * ==================
 */

var PageContent = new keystone.List('PageContent', {
	map: { name: 'elementId' },
	autokey: { from: 'elementId', path: 'key', unique: true },
	defaultColumns: 'elementId, titel, pagina',
	label: 'Content'
});

PageContent.add({
	pagina: { type: Types.Relationship, ref: 'Page' },
	elementId: { type: String, readonly: true },
	titel: { type: String },
	content: { type: Types.Html, wysiwyg: true, height: 400 },
});

PageContent.relationship({ ref: 'Page', path: 'pages', refPath: 'naam' });

PageContent.defaultColumns = 'titel|30%, elementId|20%, pagina|20%';

PageContent.register();
