var keystone = require('keystone');
var ejs = require('ejs');
var Types = keystone.Field.Types;

/**
 * Product Model
 */
var Page = new keystone.List('Page', {
	map: { naam: 'naam' },
  autokey: { path: 'slug', from: 'naam', unique: true },
  defaultColumns: 'naam, adres, actief',
	label: 'Paginas',
	singular: 'Pagina',
	plural: 'Paginas',
	sortable: true
});

Page.add({
	naam: { type: String, required: true, initial: false },
	slug: { type: String, readonly: true },
  adres: { type: Types.Url, required: true, initial: true },
  actief: { type: Boolean, default: true },
  zichtbaarInMenu: { type: Boolean, default: true },
});

Page.relationship({ ref: 'PageContent', path: 'page-contents', refPath: 'pagina' });

Page.register();
