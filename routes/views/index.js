var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'home';

	locals.data = {
		content: []
	};

	// view.query('content', keystone.list('PageContent').model.find().where('page', locals.section));

	view.on('init', function (next) {

		var q = keystone.list('PageContent').model.find().populate('page');

		q.exec(function (err, results) {
			locals.data.contents = results.filter(function(result){
				return result.page.slug == locals.section;
			});
			console.log(locals.data.contents);
			next(err);
		});

	});
	// Render the view
	view.render('index');
};
